require 'test_helper'

class ErrorControllerTest < ActionController::TestCase
  test "should get no_access" do
    get :no_access
    assert_response :success
  end

end
