class ChangeUsersAuthIdToString < ActiveRecord::Migration
  def up
    change_column :users, :auth_id, :string
  end

  def down
  end
end
