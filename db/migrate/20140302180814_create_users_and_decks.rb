class CreateUsersAndDecks < ActiveRecord::Migration
  def up
    create_table :users_decks do |t|
      t.belongs_to :user
      t.belongs_to :deck
    end
  end

  def down
  end
end
