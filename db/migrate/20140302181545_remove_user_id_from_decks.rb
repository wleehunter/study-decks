class RemoveUserIdFromDecks < ActiveRecord::Migration
  def up
    remove_column :decks, :user_id
  end

  def down
  end
end
