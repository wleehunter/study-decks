class RemoveEmailAuthIdFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :email_auth_id
  end

  def down
    add_column :users, :email_auth_id, :integer
  end
end
