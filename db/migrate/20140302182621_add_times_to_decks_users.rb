class AddTimesToDecksUsers < ActiveRecord::Migration
  def change
    add_column(:decks_users, :created_at, :datetime)
    add_column(:decks_users, :updated_at, :datetime)
  end
end
