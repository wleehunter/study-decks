class RemoveTimestampsFromDecksUsers < ActiveRecord::Migration
  def up
    remove_column(:decks_users, :created_at, :datetime)
    remove_column(:decks_users, :updated_at, :datetime)
  end

  def down
  end
end
