class RenameUsersDecks < ActiveRecord::Migration
  def up
    rename_table :users_decks, :decks_users
  end

  def down
  end
end
