class AddEmailAuthIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :email_auth_id, :integer
  end
end
