require 'json'

class UsersController < ApplicationController
  before_filter :require_email_auth, :except => [:new, :create, :account_created, :validate_email]
  before_filter :require_same_user, :only => :show
  # GET /users
  # GET /users.json
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @message = nil
    if params[:message]
      @message = MESSAGES[params[:message]]
    end

    @user = User.find(params[:id])
    @my_decks = @user.decks
    @public_decks = Deck.includes(:users).where(:private => false).where("users.id != ?", current_user.id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    @user.admin = 0;
    respond_to do |format|
      if @user.save
        @user.send_auth_email(@user)
        # format.html { redirect_to '/users/' + @user.id.to_s + '/user_created' }
        format.html { redirect_to '/account-created' }

        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def account_created

  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def validate_email
    url_params = params[:v]
    decoded = JSON.parse(Base64.decode64(url_params))
    @user = User.find(decoded["id"])
    if @user && @user.auth_id == decoded["auth_id"]
      @user.update_attribute(:auth_id, nil)
      sign_in @user
      redirect_to '/users/' + @user.id.to_s + '/email_verified'
    end
  end

end
