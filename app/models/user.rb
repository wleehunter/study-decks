require 'base64'
class User < ActiveRecord::Base
	has_secure_password
  attr_accessible :admin, :auth_id, :email, :password, :password_confirmation, :username

  has_and_belongs_to_many :decks

  validates :username, presence: true, length: { maximum: 50 },
            uniqueness: {case_sensitive: false}

  validates :email, presence: true, 
      			uniqueness: {case_sensitive: false}

  validates :password, presence: true, length: { minimum: 6 }
  validates :password_confirmation, presence: true


  before_validation :create_auth_id, :on => :create
  before_save { |user| user.email = email.downcase }
  before_save :create_remember_token

  def send_auth_email  user
    params = {"id" => user.id, "auth_id" => user.auth_id}.to_json
    params_encoded = Base64.encode64(params)
    EmailAuthMailer.send_auth_email(user, params_encoded).deliver
  end

  protected

  def create_auth_id
    begin
       self.auth_id = SecureRandom.urlsafe_base64(nil, false)
    end while User.where(auth_id: auth_id).exists?
  end

  private

  def create_remember_token
    self.remember_token = SecureRandom.urlsafe_base64
  end

end
