class Deck < ActiveRecord::Base
  attr_accessible :icon_path, :name, :private
  has_and_belongs_to_many :users
end
