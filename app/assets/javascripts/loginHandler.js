(function(d) {
  d.loginHandler = {
    $loginTrigger: null,
    $loginForm: null,
    init: function() {
      var login = this;
      this.$loginTrigger = $('.login-trigger');
      this.$loginForm = $('.login-form');

      this.$loginTrigger.click(function(){
        login.onLoginTriggerClick();
      })
    },

    onLoginTriggerClick: function() {
      this.$loginForm.toggleClass('hidden');
    }
  }
})(window.decks);