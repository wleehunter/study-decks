class EmailAuthMailer < ActionMailer::Base
  default from: "studydecks@gmail.com"
  @@BASE_URL = 'http://localhost:3000/validate_email/'
  #@@BASE_URL = 'https://studydecks.herokuapp.com/validate_email/'

  def send_auth_email user, params
    @username = user.username
    @verification_url = @@BASE_URL + params
    mail(:to => user.email, :subject => "Email confirmation for Study Decks.")
  end
end
